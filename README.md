# Schema Registry

Just a project stub for now

Contributions are welcome. Just get in touch.

## Quickstart

Simply `pip install schema-registry` and get going.

## Development

This project uses `poetry` for dependency management and `pre-commit` for local checks.
